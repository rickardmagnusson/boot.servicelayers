﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Boot.ServiceLayers.Models;

namespace Boot.ServiceLayers.Controllers
{
    public class BaseApiController<T> : ApiController
    {
        private readonly dynamic _md;

        protected BaseApiController()
        {
            _md = (IEnumerable<T>)new Repository().DataList;
        }

        public IEnumerable<T> Get()
        {
            return _md;
        }

        public T Get(int id)
        {
            return (T)Get().Cast<IMD>().First<IMD>(t => (t.Id == id));
        }
    }
}
