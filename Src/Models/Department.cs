﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Boot.ServiceLayers.Extensions;

namespace Boot.ServiceLayers.Models
{
    //Hierarchial test
    public class Department : IHierarchy<Department>
    {
        public Int32 Id { get; set; }
        public string Name { get; set; }
        public ICollection<Department> Collection { get; set; }
    }

    public class Group : IHierarchy<Group>
    {
        public Int32 Id { get; set; }
        public string Name { get; set; }
        public ICollection<Group> Collection { get; set; }
    }

    public interface IHierarchy<T>
    {
        Int32 Id { get; set; }
        string Name { get; set; }
        ICollection<T> Collection { get; set; } 
    }

    public interface IHierarchial
    {
        string Construct();
    }



    //Test repository
    public class StaffHierarchy : IHierarchial
    {
        public Collection<Department> Staff { get; set; }

        public StaffHierarchy()
        {
            Staff = new Collection<Department>()
            {
                new Department(){Id=0, Name = "Staff Root", Collection = new Collection<Department>()
                {
                    new Department(){Id=1, Name = "Owners", Collection = new Collection<Department>()
                    {
                        new Department(){Id=2, Name = "CEO's", Collection = new Collection<Department>()
                        {
                            new Department(){Id=3, Name = "Developer personell", Collection = new Collection<Department>()},
                            new Department(){Id=4, Name = "Marketing personell", Collection = new Collection<Department>()},
                            new Department(){Id=5, Name = "Financial personell", Collection = new Collection<Department>()}
                        },}}}}}
            };
        }

        public string Construct()
        {
            return Staff.HierarchicalView(r => r.Collection, r => r.Name);
        }
    }
}