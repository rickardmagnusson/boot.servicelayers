﻿
using System.Collections.Generic;

namespace Boot.ServiceLayers.Models
{
    public class Repository
    {
        public dynamic DataList;

        public Repository()
        {
            DataList = new List<MD>()
            {
                new MD {Id = 1, Html = "Test 1"},
                new MD {Id = 2, Html = "Test 2"},
                new MD {Id = 1, Html = "Test 3"},
                new MD {Id = 1, Html = "Test 4"},
                new MD {Id = 1, Html = "Test 5"},
                new MD {Id = 1, Html = "Test 6"},
                new MD {Id = 1, Html = "Test 7"}
            };
        }
    }
}