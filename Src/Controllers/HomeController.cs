﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Boot.ServiceLayers.Extensions;
using Boot.ServiceLayers.Models;

namespace Boot.ServiceLayers.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View(new IndexModel());
        }

        public class IndexModel
        {
            public string Tree { get; private set; }
            public IndexModel()
            {
                Tree = new StaffHierarchy().Construct();
            }
        }
    }
}
