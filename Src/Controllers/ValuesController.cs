﻿

using System.Web.Http;
using Boot.ServiceLayers.Models;

namespace Boot.ServiceLayers.Controllers
{
    public class ValuesController : ApiController
    {
        public string GetData()
        {
            return new StaffHierarchy().Construct();
        }
    }
}
