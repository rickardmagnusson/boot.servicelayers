﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Boot.ServiceLayers.Extensions
{
    public static class ListExtensions
    {



        /// <summary>
        /// Eg. myList.Flatten(i => i.SubEntries);
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sequence"></param>
        /// <param name="childFetcher"></param>
        /// <returns></returns>
        public static IEnumerable<T> Flatten<T>(this IEnumerable<T> sequence, Func<T, IEnumerable<T>> childFetcher)
        {
            var itemsToYield = new Queue<T>(sequence);
            while (itemsToYield.Count > 0)
            {
                var item = itemsToYield.Dequeue();
                yield return item;

                var children = childFetcher(item);

                if (children == null) continue;

                foreach (var child in children)
                    itemsToYield.Enqueue(child);
            }
        }



        /// <summary>
        ///  var staff = new StaffHierarchy().Staff;
        ///  var Tree = staff.HierarchicalView(r => r.Departments, r => r.DepName);
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="rootItems"></param>
        /// <param name="childrenProperty"></param>
        /// <param name="itemContent"></param>
        /// <returns></returns>
        public static string HierarchicalView<T>(this IEnumerable<T> rootItems, Func<T, IEnumerable<T>> childrenProperty, Func<T, string> itemContent)
        {
            var enumerable = rootItems as IList<T> ?? rootItems.ToList();
            if (rootItems == null || !enumerable.Any()) return null;

            var builder = new StringBuilder();
            builder.AppendLine("<ul>");

            foreach (var item in enumerable)
            {
                builder.Append("  <li>").Append(itemContent(item)).AppendLine("</li>");
                var childContent = HierarchicalView(childrenProperty(item), childrenProperty, itemContent);

                if (childContent == null) continue;
                var indented = childContent.Replace(System.Environment.NewLine, System.Environment.NewLine + "  ");
                builder.Append("  ").AppendLine(indented);
            }

            builder.Append("</ul>");
            return builder.ToString();
        }
    }
}