﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Boot.ServiceLayers.Models
{
    public class MD : IMD
    {
        public Int32 Id { get; set; }
        public string Html { get; set; }
    }

    public interface IMD
    {
        Int32 Id { get; set; }
    }
}